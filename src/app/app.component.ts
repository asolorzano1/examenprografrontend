import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title:string = 'EXAMEN';
  msg:string = '';

  empleados = [
    {'name': 'Angel', position: 'Programador'},
    {'name': 'Jorge', position: 'Diseñador'}
  ];

  model:any = {};
  model2:any = {};
  hideUpdate: boolean = true;

  addEmpleado():void{
    this.empleados.push(this.model);
    this.msg = 'Datos agregados';
  }

  deleteEmpleado(i: number):void{
    var answer = confirm('Estas seguro');
    if(answer){
      this.empleados.splice(i, 1);
      this.msg = 'Datos eliminados';
    }
  }

  // @ts-ignore
  myvalue;
  editEmpleado(i: number):void{
    this.hideUpdate = false;
    this.model2.name = this.empleados[i].name;
    this.model2.position = this.empleados[i].position;
    this.myvalue = i;
  }

  updateEmpleado():void{
    let i = this.myvalue;
    for(let j = 0; j< this.empleados.length; j++){
      if( i == j){
        this.empleados[i] = this.model2;
        this.msg = 'Datos actualizados';
        this.model2 = {};
      }
    }
  }

  closeAlert() {
    this.msg = '';
  }
}
